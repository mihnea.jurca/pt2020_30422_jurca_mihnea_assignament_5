package com.utcn.Assignament_5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
 

public class Task1 {
	ArrayList<MonitoredData> list;
	
	public Task1(ArrayList<MonitoredData> list){
		this.list = list;
		try{
			  // Create file 
			  FileWriter fstream = new FileWriter("/Users/mihneajurk/Desktop/PT2020_30422_Jurca_Mihnea_Assignament5/PT2020_30422_Jurca_Mihnea_Assignament5/Task_1.txt");
			  BufferedWriter out = new BufferedWriter(fstream);
			  for(MonitoredData it : list){
				  out.write(it.toString());
			  	  out.write("\n");
			  }
			  //Close the output stream
			  out.close();
			  }catch (Exception e){//Catch exception if any
			  System.err.println("Error: " + e.getMessage());
			  }
	}
}
