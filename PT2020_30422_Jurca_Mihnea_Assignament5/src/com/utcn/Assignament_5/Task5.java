package com.utcn.Assignament_5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Map;

public class Task5 {
private Map<String, Long> map;
	
	public Task5(Map<String, Long> rezult2) {
		// TODO Auto-generated constructor stub
		this.map = rezult2;
		try{
			  // Create file 
			  FileWriter fstream = new FileWriter("/Users/mihneajurk/Desktop/PT2020_30422_Jurca_Mihnea_Assignament5/PT2020_30422_Jurca_Mihnea_Assignament5/Task_5.txt");
			  BufferedWriter out = new BufferedWriter(fstream);
			  for (String name: rezult2.keySet()){
		            String key = name.toString();
		            int valueDays = (int) (map.get(name)/ 86400);  
		            int valueHours = (int) ((map.get(name) - (86400*valueDays)) / 3600);
		            int valueMinutes = (int) ((map.get(name) - (86400*valueDays) - 3600*valueHours) / 60);
		            int valueSeconds = (int) (map.get(name) - (86400*valueDays) - 3600*valueHours - 60*valueMinutes);
		            out.write(key + " " + valueDays + ":" + valueHours + ":" + valueMinutes + ":" + valueSeconds); 
		            out.write("\n");
		} 
			  //Close the output stream
			  out.close();
			  }catch (Exception e){//Catch exception if any
			  System.err.println("Error: " + e.getMessage());
			  }
	}
}
