package com.utcn.Assignament_5;


import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MonitoredData {
	
	private String activity;
	private Date start_time;
	private Date end_time;
	private LocalTime totalTime;
	
	
	public MonitoredData(String activity, Date start_time, Date end_time) {
		super();
		this.activity = activity;
		this.start_time = start_time;
		this.end_time = end_time;
	}
	

	@Override
	public String toString() {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return format.format(start_time) + "     " +  format.format(end_time) +  "     "  + activity;
	}


	public String getActivity() {
		return activity;
	}


	public void setActivity(String activity) {
		this.activity = activity;
	}


	public Date getStart_time() {
		return start_time;
	}


	public void setStart_time(Date start_time) {
		this.start_time = start_time;
	}


	public Date getEnd_time() {
		return end_time;
	}


	public void setEnd_time(Date end_time) {
		this.end_time = end_time;
	}
	
	public String dayOfDate(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(getStart_time());
	}
	
	public Duration computeTime(Date beginTime, Date endTime) 
    {
		
		Calendar cal1 = Calendar.getInstance();
	    cal1.setTime(beginTime);
	    
	    Calendar cal2 = Calendar.getInstance();
	    cal2.setTime(endTime);
	    
		int yearBegin = cal1.get(Calendar.YEAR);
		int yearEnd = cal2.get(Calendar.YEAR);

		int monthBegin = cal1.get(Calendar.MONTH) + 1;
		int monthEnd = cal2.get(Calendar.MONTH) + 1;
		
		int dayBegin = cal1.get(Calendar.DAY_OF_MONTH);
		int dayEnd = cal2.get(Calendar.DAY_OF_MONTH);
		
		int hourBegin = cal1.get(Calendar.HOUR_OF_DAY);
		int hourEnd = cal2.get(Calendar.HOUR_OF_DAY);
		
		int minutesBegin = cal1.get(Calendar.MINUTE);		
		int minutesEnd = cal2.get(Calendar.MINUTE);
		
		int secondsBegin = cal1.get(Calendar.SECOND);
		int secondsEnd = cal2.get(Calendar.SECOND);		
		
		LocalDateTime fromDateTime = LocalDateTime.of(yearBegin, monthBegin, dayBegin, hourBegin, minutesBegin, secondsBegin);
		LocalDateTime toDateTime = LocalDateTime.of(yearEnd, monthEnd, dayEnd, hourEnd, minutesEnd, secondsEnd);
		
		Duration durResult = Duration.between(fromDateTime, toDateTime);

		return durResult;

    }
	
	
	
	
	
	
	

}
