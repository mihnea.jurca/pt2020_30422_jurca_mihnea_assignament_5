package com.utcn.Assignament_5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Map;

public class Task4 {
	private Map<Integer, Map<String, Long>> map;
	
	public Task4(Map<Integer, Map<String, Long>> rezult2) {
		// TODO Auto-generated constructor stub
		this.map = rezult2;
		try{
			  // Create file 
			  FileWriter fstream = new FileWriter("/Users/mihneajurk/Desktop/PT2020_30422_Jurca_Mihnea_Assignament5/PT2020_30422_Jurca_Mihnea_Assignament5/Task_4.txt");
			  BufferedWriter out = new BufferedWriter(fstream);
			  for (Integer name: rezult2.keySet()){
		            String key = name.toString();
		            String value = map.get(name).toString();  
		            out.write(key + " " + value); 
		            out.write("\n");
		} 
			  //Close the output stream
			  out.close();
			  }catch (Exception e){//Catch exception if any
			  System.err.println("Error: " + e.getMessage());
			  }
	}
}
