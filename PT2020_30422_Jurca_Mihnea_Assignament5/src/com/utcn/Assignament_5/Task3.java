package com.utcn.Assignament_5;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Map;

public class Task3 {
	Map<String, Long> result;
	
	public Task3(Map<String, Long> result2) {
		// TODO Auto-generated constructor stub
		this.result = result2;
		try{
			  // Create file 
			  FileWriter fstream = new FileWriter("/Users/mihneajurk/Desktop/PT2020_30422_Jurca_Mihnea_Assignament5/PT2020_30422_Jurca_Mihnea_Assignament5/Task_3.txt");
			  BufferedWriter out = new BufferedWriter(fstream);
			  for (String name: result.keySet()){
		            String key = name.toString();
		            String value = result.get(name).toString();  
		            out.write(key + " " + value); 
		            out.write("\n");
		} 
			  //Close the output stream
			  out.close();
			  }catch (Exception e){//Catch exception if any
			  System.err.println("Error: " + e.getMessage());
			  }
	}

	
}
